/**
 * @file panda_driver.cpp
 * @author Benjamin Navarro
 * @brief Defines a simple Franka Panda driver
 * @date 17-07-2019
 * License: CeCILL
 */

#include <rkcl/drivers/panda_driver.h>
#include <rkcl/processors/dsp_filters/butterworth_low_pass_filter.h>

#include <franka/control_types.h>
#include <franka/duration.h>
#include <franka/robot.h>
#include <franka/robot_state.h>
#include <franka/model.h>

#include <yaml-cpp/yaml.h>

#include <chrono>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <algorithm>
#include <atomic>

namespace rkcl
{

bool PandaDriver::registered_in_factory = DriverFactory::add<PandaDriver>("panda");

struct PandaDriver::pImpl
{
    pImpl(rkcl::JointGroupPtr joint_group, rkcl::ObservationPointPtr obs_pt, const std::string& ip_address)
        : joint_group_(joint_group),
          obs_pt_(obs_pt),
          franka_(ip_address)
    {
        stop_.store(false);
        wrench_deadband_.setZero();
    }

    void enableWrenchFiltering(int order, double cutoff_frequency)
    {
        wrench_filter_ = std::make_unique<rkcl::ButterworthLowPassFilter>(order, 1000., cutoff_frequency);
        wrench_filter_->setData(obs_pt_->state.wrench);
        wrench_filter_->init();
    }

    void enableWrenchDeadband(const Eigen::Matrix<double, 6, 1>& deadband)
    {
        wrench_deadband_ = deadband;
    }

    void init()
    {
        franka_.setCollisionBehavior({{100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0}},
                                     {{100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0}},
                                     {{100.0, 100.0, 100.0, 100.0, 100.0, 100.0}},
                                     {{100.0, 100.0, 100.0, 100.0, 100.0, 100.0}});
        franka::Model model = franka_.loadModel();
        auto state = franka_.readOnce();
        updateFromState(state);
    }

    void start()
    {
        control_thread_ = std::thread([this] {
            franka_.control([&](const franka::RobotState& state,
                                franka::Duration period) -> franka::JointVelocities {
                franka::JointVelocities velocities{{0, 0, 0, 0, 0, 0, 0}};

                sync_pred_ = true;
                sync_signal_.notify_one();

                if (stop_.load())
                {
                    return franka::MotionFinished(velocities);
                }
                else
                {
                    std::lock_guard<std::mutex> lock(mtx_);

                    setCommand(velocities);
                    updateFromState(state);

                    return velocities;
                }
            });
        });
    }

    void stop()
    {
        stop_.store(true);
        control_thread_.join();
    }

    void sync()
    {
        std::unique_lock<std::mutex> lock(sync_mtx_);
        sync_signal_.wait(lock, [this] { return sync_pred_; });
        sync_pred_ = false;
    }

    void read()
    {
        std::unique_lock<std::mutex> lock1(mtx_, std::defer_lock);
        std::unique_lock<std::mutex> lock2(joint_group_->state_mtx, std::defer_lock);
        std::lock(lock1, lock2);

        joint_group_->state.position = internal_state_.joint_current_position;
        joint_group_->state.velocity = internal_state_.joint_current_velocity;
        joint_group_->state.force = internal_state_.joint_external_torque;

        joint_group_->last_state_update = std::chrono::high_resolution_clock::now();

        if (obs_pt_)
        {
            std::unique_lock<std::mutex> lock3(obs_pt_->state_mtx);

            // The measured force is the force applied by the environment, not
            // the one applied by the robot to the environment
            obs_pt_->state.wrench = -internal_state_.control_point_external_force;
            obs_pt_->state.wrench.deadbandInPlace(wrench_deadband_);
            if (wrench_filter_)
            {
                wrench_filter_->process();
            }
        }
    }

    void send()
    {
        std::unique_lock<std::mutex> lock1(mtx_, std::defer_lock);
        std::unique_lock<std::mutex> lock2(joint_group_->command_mtx, std::defer_lock);
        std::lock(lock1, lock2);

        internal_state_.joint_command_velocity = joint_group_->command.velocity;
        joint_group_->command.position = joint_group_->command.position + joint_group_->command.velocity * joint_group_->control_time_step;
    }

private:
    struct InternalState
    {
        InternalState()
        {
            joint_current_position.setZero();
            joint_current_velocity.setZero();
            joint_command_velocity.setZero();
            joint_external_torque.setZero();
            control_point_external_force.setZero();
        }

        Eigen::Matrix<double, 7, 1> joint_current_position;
        Eigen::Matrix<double, 7, 1> joint_current_velocity;
        Eigen::Matrix<double, 7, 1> joint_command_velocity;
        Eigen::Matrix<double, 7, 1> joint_external_torque;
        Eigen::Matrix<double, 6, 1> control_point_external_force;
    };

    void updateFromState(const franka::RobotState& state)
    {
        std::copy_n(state.theta.begin(), 7, internal_state_.joint_current_position.data());
        std::copy_n(state.dtheta.begin(), 7, internal_state_.joint_current_velocity.data());
        std::copy_n(state.tau_ext_hat_filtered.begin(), 7, internal_state_.joint_external_torque.data());
        std::copy_n(state.K_F_ext_hat_K.begin(), 6, internal_state_.control_point_external_force.data());
    }

    void setCommand(franka::JointVelocities& velocities)
    {
        std::copy_n(internal_state_.joint_command_velocity.data(), 7, velocities.dq.begin());
    }

    InternalState internal_state_;
    rkcl::JointGroupPtr joint_group_;
    rkcl::ObservationPointPtr obs_pt_;
    std::unique_ptr<rkcl::ButterworthLowPassFilter> wrench_filter_;
    Eigen::Matrix<double, 6, 1> wrench_deadband_;
    franka::Robot franka_;
    std::mutex mtx_;
    std::atomic_bool stop_;
    std::thread control_thread_;
    std::condition_variable sync_signal_;
    std::mutex sync_mtx_;
    bool sync_pred_;
};

PandaDriver::PandaDriver(const std::string& ip_address, JointGroupPtr joint_group, ObservationPointPtr op_eef)
    : Driver(joint_group), op_eef_(op_eef)
{
    impl_ = std::make_unique<PandaDriver::pImpl>(joint_group_, op_eef_, ip_address);
}

PandaDriver::PandaDriver(const std::string& ip_address, JointGroupPtr joint_group)
    : PandaDriver(ip_address, joint_group, nullptr)
{
}

PandaDriver::PandaDriver(Robot& robot, const YAML::Node& configuration)
    : op_eef_(nullptr)
{
    std::cout << "Configuring Franka Panda driver..." << std::endl;
    if (configuration)
    {
        std::string ip_address;
        try
        {
            ip_address = configuration["ip_address"].as<std::string>();
        }
        catch (...)
        {
            throw std::runtime_error(
                "PandaDriver::PandaDriver: You must provide an 'ip_address' "
                "field in the driver configuration.");
        }

        std::string joint_group;
        try
        {
            joint_group = configuration["joint_group"].as<std::string>();
        }
        catch (...)
        {
            throw std::runtime_error("PandaDriver::PandaDriver: You "
                                     "must provide a 'joint_group' field");
        }
        joint_group_ = robot.getJointGroupByName(joint_group);
        if (not joint_group_)
            throw std::runtime_error("PandaDriver::PandaDriver: "
                                     "unable to retrieve joint group " +
                                     joint_group);

        auto op_name = configuration["end-effector_point_name"];
        if (op_name)
        {
            op_eef_ = robot.getObservationPointByName(op_name.as<std::string>());
            if (not op_eef_)
                throw std::runtime_error("PandaDriver::PandaDriver: unable to retrieve "
                                         "end-effector body name");
        }

        impl_ = std::make_unique<PandaDriver::pImpl>(joint_group_, op_eef_, ip_address);

        double wrench_cutoff_frequency = configuration["wrench_cutoff_frequency"].as<double>(-1);
        double wrench_filter_order = configuration["wrench_filter_order"].as<int>(4);

        if (wrench_cutoff_frequency >= 0.)
        {
            impl_->enableWrenchFiltering(wrench_filter_order, wrench_cutoff_frequency);
        }

        if (configuration["wrench_deadband"])
        {
            try
            {
                Eigen::Matrix<double, 6, 1> wrench_deadband(configuration["wrench_deadband"].as<std::array<double, 6>>().data());
                impl_->enableWrenchDeadband(wrench_deadband);
            }
            catch (...)
            {
                throw std::runtime_error(
                    "PandaDriver::PandaDriver: the 'wrench_deadband' field "
                    "must be a 6 components vector");
            }
        }
    }
    else
    {
        throw std::runtime_error(
            "PandaDriver::PandaDriver: The configuration file "
            "doesn't include a 'driver' field.");
    }
}

PandaDriver::~PandaDriver() = default;

void PandaDriver::enableWrenchFiltering(int order, double cutoff_frequency)
{
    impl_->enableWrenchFiltering(order, cutoff_frequency);
}
void PandaDriver::enableWrenchDeadband(const Eigen::Matrix<double, 6, 1>& deadband)
{
    impl_->enableWrenchDeadband(deadband);
}

bool PandaDriver::init(double timeout)
{
    impl_->init();
    return true;
}

bool PandaDriver::start()
{
    impl_->start();
    return true;
}

bool PandaDriver::stop()
{
    sync();
    impl_->stop();
    return true;
}

bool PandaDriver::read()
{
    impl_->read();
    return true;
}

bool PandaDriver::send()
{
    impl_->send();
    return true;
}

bool PandaDriver::sync()
{
    impl_->sync();
    return true;
}

} // namespace rkcl